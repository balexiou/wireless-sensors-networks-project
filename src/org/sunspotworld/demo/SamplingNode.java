/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.sunspotworld.demo;

import java.lang.Math;
import java.util.Random;
import org.encog.neural.data.NeuralData; 
import org.encog.neural.data.NeuralDataPair; 
import org.encog.neural.data.NeuralDataSet;
import org.encog.neural.data.basic.BasicNeuralDataSet;
import org.encog.neural.data.basic.BasicNeuralDataSet.BasicNeuralIterator;
import org.encog.neural.data.basic.BasicNeuralDataPair;
import org.encog.neural.data.basic.UnsupportedOperationException;
import org.encog.neural.networks.BasicNetwork;
import org.encog.neural.networks.Train; 
import org.encog.neural.networks.layers.FeedforwardLayer; 
import org.encog.neural.networks.training.backpropagation.Backpropagation;
import com.sun.spot.io.j2me.radiogram.RadiogramConnection;
import com.sun.spot.peripheral.NoAckException;
import com.sun.spot.resources.Resources;
import com.sun.spot.util.Utils;
import java.io.IOException;
import javax.microedition.io.Connector;
import javax.microedition.io.Datagram;
import javax.microedition.midlet.MIDlet;
import javax.microedition.midlet.MIDletStateChangeException;

/**
 *
 * @author Babis Alexiou
 */
public class SamplingNode extends MIDlet {
    
    public static double SAMPLE_INPUT[][] = { {35.0, 35.0}, {36.0, 34.0}, 
        {34.0, 36.0}, {34.0, 34.0} };
    
    public static double SAMPLE_IDEAL[][] = { { 1.0 }, {1.0}, {0.0}, {0.0} };
    
    static long seed = System.currentTimeMillis();
    static Random rnd = new Random(seed);
    
    public static double sqrt(double num) {
        
        double t;
        double squareRoot = num/2;
        do {
            t = squareRoot;
            squareRoot = (t+(num/t))/2;
        }while ((t-squareRoot) != 0);
        
        return squareRoot;
        
    }
    
    public static double ln(double x) {
        
        double i, logx, logy, ty, tty;
        i = 1;
        logx = 0;
        ty = (x-1)/(x+1);
        do{
            logx = logx + ty/i;
            tty = ty;
            ty = (ty * ((x-1)/(x+1)) * ((x-1)/(x+1)));
            i+=2;
        }while (tty - ty > 0.0000005);
        
        return 2*logx;
        
    }
    
    
    static private double x1, x2;
    static boolean haveNextGaussian = false;
    
    // Mean mu, standard deciation sigma
    
    static public double nextGaussian_sample(double mu, double sigma) {
        
        double u1,u2,w,mult;
        if (haveNextGaussian){
            haveNextGaussian = false;
            //System.out.println((mu + sigma * x2));
            return (mu + sigma * x2);
        }
        else {
            do {
                u1 = 2 * rnd.nextDouble() - 1;
                u2 = 2 * rnd.nextDouble() - 1;
                w = u1 * u1 + u2 * u2;
            }while(w >= 1 || w == 0);
            mult = sqrt(((-2 * ln(w))/w));
            x1 = u1 * mult;
            x2 = u2 * mult;
            haveNextGaussian = true;
            //System.out.println((mu + sigma * x1));
            return (mu + sigma * x1);
        }
    }
    
    public static double [][] Sample(double mu, double sigma) {
        
        double temperature = nextGaussian_sample(mu, sigma);
        double humidity = nextGaussian_sample(mu, sigma);
        
        double Samples[][] = { { temperature, humidity } };
        
        return Samples;
    }
    
    RadiogramConnection conn;
    
    public void run() throws IOException {
        
        String address = "0a00.020f.0000.1001";
        //System.out.println(name);
        double mu = 35.0;
        double sigma = 1.0;
        conn = (RadiogramConnection)Connector.open("radiogram://"+address+":100");
        
        System.out.println("Building the Neural Network...");
        
        BasicNetwork network =  new BasicNetwork();
        network.addLayer(new FeedforwardLayer(2));
        network.addLayer(new FeedforwardLayer(4));
        network.addLayer(new FeedforwardLayer(1));
        network.reset();
        
        NeuralDataSet trainingSet = new BasicNeuralDataSet(SAMPLE_INPUT, 
                SAMPLE_IDEAL);
        
        System.out.println("Training the Neural Network...");
        
        final Train train;
        train = new Backpropagation(network, trainingSet, 0.7, 0.9);
        
        int epoch = 1; 
 
        do { 
            /*if (epoch > 400) {
                System.out.println("Resetting the Network!");
                network.reset();
                epoch = 1;
            }*/
            train.iteration(); 
            System.out .println("Epoch #" + epoch + " Error:" + train.getError()); 
            epoch++; 
        } while ((epoch < 400) && (train.getError() > 0.001));
        
        System.out.println("Neural Network Results:");
        
        BasicNeuralDataSet trainingset = new BasicNeuralDataSet(SAMPLE_INPUT, 
                SAMPLE_IDEAL);
         
        BasicNeuralDataSet.BasicNeuralIterator itr = trainingset.new BasicNeuralIterator();
        while (itr.hasNext()){
            NeuralDataPair pair = itr.next();
            final NeuralData output = network.compute(pair.getInput()); 
            System.out.println(pair.getInput().getData(0) + "," + pair.getInput().getData(1) 
                + ", actual=" + output.getData(0) + ",ideal=" + pair.getIdeal().getData(0));
        }
        
        
        while (true) {
            
            Datagram dg = conn.newDatagram(conn.getMaximumLength());
            double data[][] = Sample(mu, sigma);
            double ideal[][] = {{0.0}, {1.0}};
            
            BasicNeuralDataSet testSet = new BasicNeuralDataSet(data, 
                ideal);
            
            BasicNeuralDataSet.BasicNeuralIterator itr1 = testSet.new BasicNeuralIterator();
            int vote = -1;
            while (itr1.hasNext()){
                NeuralDataPair pair = itr1.next();
                final NeuralData output = network.compute(pair.getInput()); 
                System.out.println(pair.getInput().getData(0) + "," + pair.getInput().getData(1) 
                    + ", actual=" + output.getData(0));
                if (output.getData(0) > 0.5){
                    vote = 1;
                }
                else
                    vote = 0;
            }
            System.out.println("Vote is: " + vote);
            
            try {
                dg.writeInt(vote);
                conn.send(dg);
            }catch (NoAckException e) {
                System.out.println("No reply from "+address);
            }finally {
                    conn.close();
                }
            Utils.sleep(4000);
        }
        
    }
    
    /**
     * MIDlet call to start our application.
     */
    protected void startApp() throws MIDletStateChangeException {
        // Listen for downloads/commands over USB connection
        new com.sun.spot.service.BootloaderListenerService().getInstance().start();
        try {
            run();
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }

    protected void pauseApp() {
        // This will never be called by the Squawk VM
    }

    /**
     * Called if the MIDlet is terminated by the system.
     * @param unconditional If true the MIDlet must cleanup and release all resources.
     */
    protected void destroyApp(boolean unconditional) throws MIDletStateChangeException {
        try {
            conn.close();
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }
    
}
