/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.sunspotworld.demo;

import com.sun.spot.io.j2me.radiogram.RadiogramConnection;
import com.sun.spot.peripheral.NoAckException;
import com.sun.spot.resources.Resources;
import com.sun.spot.util.Utils;
import java.io.IOException;
import java.util.Random;
import java.lang.Math;
import javax.microedition.io.Connector;
import javax.microedition.io.Datagram;
import javax.microedition.midlet.MIDlet;
import javax.microedition.midlet.MIDletStateChangeException;
import com.sun.spot.resources.Resources;
import com.sun.spot.resources.transducers.ITriColorLEDArray;
import com.sun.spot.resources.transducers.LEDColor;
import com.sun.spot.util.Utils;

/**
 *
 * @author Babis Alexiou
 */
public class Sink extends MIDlet {
    
    private ITriColorLEDArray leds = (ITriColorLEDArray) Resources.lookup(ITriColorLEDArray.class);
    
    public void receive_dg() throws IOException {
        RadiogramConnection conn = (RadiogramConnection)Connector.open("radiogram://:101");
        while(true){
            int alert = -1;
            try {
                if(conn.packetsAvailable())
                {
                    System.out.println("Datagram Packets Available!");
                    Datagram dg = conn.newDatagram(conn.getMaximumLength());
                    conn.receive(dg);
                    alert = dg.readInt();
                    if (alert == 1) {
                        System.out.println("Wildfire Detected!");
                        for (int i = 0; i < leds.size(); i++) {
                            leds.getLED(i).setColor(LEDColor.RED);
                            leds.getLED(i).setOn();
                            Utils.sleep(200);                   // on for 1/5 second
                            leds.getLED(i).setOff();
                        }
                    }
                    else {
                        System.out.println("Everything is Normal!");
                        for (int i = 0; i < leds.size(); i++) {
                            leds.getLED(i).setColor(LEDColor.GREEN);
                            leds.getLED(i).setOn();
                            Utils.sleep(200);                   // on for 1/5 second
                            leds.getLED(i).setOff();
                        }
                    }
               }
               else
                    System.out.println("Idle");
            }catch (NoAckException e) {
                
            }finally {
                //conn.close();
            }
            Utils.sleep(4000);
        }
    }
    
    /**
     * MIDlet call to start our application.
     */
    protected void startApp() throws MIDletStateChangeException {
        // Listen for downloads/commands over USB connection
        new com.sun.spot.service.BootloaderListenerService().getInstance().start();
        
        try {
            receive_dg();
        } catch (IOException ex) {
            ex.printStackTrace();
        }
       
    }

    protected void pauseApp() {
        // This will never be called by the Squawk VM
    }

    /**
     * Called if the MIDlet is terminated by the system.
     * @param unconditional If true the MIDlet must cleanup and release all resources.
     */
    protected void destroyApp(boolean unconditional) throws MIDletStateChangeException {
        
    }
    
}
