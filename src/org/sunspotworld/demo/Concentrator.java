/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.sunspotworld.demo;

import com.sun.spot.io.j2me.radiogram.RadiogramConnection;
import com.sun.spot.peripheral.NoAckException;
import com.sun.spot.util.Utils;
import java.lang.String;
import java.io.IOException;
import javax.microedition.io.Connector;
import javax.microedition.io.Datagram;
import javax.microedition.midlet.MIDlet;
import javax.microedition.midlet.MIDletStateChangeException;

/**
 *
 * @author Babis Alexiou
 */
public class Concentrator extends MIDlet {
    
    private int[] votes_array;
    private int n;
    
    public Concentrator() {
        
        this.n = 3;
        votes_array = new int[n];
        for (int i = 0; i < n; i++) {
            votes_array[i] = -1;
        }
        
    }
    
    public void collector() throws IOException{
        
        while(true) {
            RadiogramConnection conn = (RadiogramConnection)Connector.open("radiogram://:100");
            int vote = -1;
            try {
                Datagram dg = conn.newDatagram(conn.getMaximumLength());
                for (int i = 0; i < n; i++) {
                    conn.receive(dg);
                    vote = dg.readInt();
                    String sender = dg.getAddress();
                    System.out.println(sender);
                    int pos;
                    
                    if (vote != -1) {
                        System.out.println("Received the following vote: " 
                                + vote +" from " + sender);
                        int j=0,dot=0;
                    while(dot<3){
                        char c = sender.charAt(j);
                        if(c == '.'){
                            dot++;
                        }
                        if(dot == 3){
                            String arrPos;
                            arrPos= sender.substring(j+2);
                            pos = Integer.parseInt(arrPos);
                            pos -=3;//for concetrator and sink
                            votes_array[pos] = vote;
                        }
                        j++;
                    }
                    }
                    else
                        System.out.println("No vote received!");
                }
            }catch (NoAckException e) {
                System.out.println("No reply!");
            }finally {
                conn.close();
            }
            
            for (int k = 0; k < n; k++) {
                System.out.println(votes_array[k]);
            }
        }
    }
    
    public void forwarder() throws IOException {
        
        boolean flag = false;
        int vote_yes = 0;
        int vote_no = 0;
        int final_vote = -1;
        String address = "0a00.020f.0000.1002";
        RadiogramConnection conn;
        while(true) {
            
            if(!flag) {
                
                for (int j = 0; j < n; j++){
                    System.out.println("Votes_array["+j+"]: " + votes_array[j]);
                }
                for (int k = 0; k < n; k++) {
                    if (votes_array[k] == -1) {
                        flag = false;
                    }
                    else
                        flag = true;
                }
            }
            else {
                for (int k = 0; k < n; k++) {
                    if (votes_array[k] == 1) {
                        vote_yes++;
                    }
                    else
                        vote_no++;
                }
                if (vote_yes > vote_no)
                    final_vote = 1;
                else
                    final_vote = 0;
                
                conn = (RadiogramConnection)Connector.open("radiogram://"
                        + address + ":101");
                Datagram dg = conn.newDatagram(conn.getMaximumLength());
                try {
                    dg.writeInt(final_vote);
                    conn.send(dg);
                    System.out.println("Concentrator just send the majority vote " 
                            + final_vote);
                    final_vote = -1;
                    conn.close();
                }catch (Exception e) {
                    System.out.println("Error! Concentrator could not send dg");
                }
                finally {
                    conn.close();
                }
            }
            Utils.sleep(4000);
        }
    }
    
    /**
     * MIDlet call to start our application
    */
    
    protected void startApp() throws MIDletStateChangeException {
        // Listen for downloads/commands over USB connection
        new com.sun.spot.service.BootloaderListenerService().getInstance().start();
        Concentrator C = new Concentrator();
        collThread T = new collThread(C, "collector");
        T.start();
        try {
            C.forwarder();
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }
    
    protected void pauseApp() {
        // This will never be called by the Squawk VM
    }
    
    /**
     * Called if the MIDlet is terminated by the system.
     * @param unconditional If true the MIDlet must cleanup and release all resources.
     */
    
    protected void destroyApp(boolean unconditional) throws MIDletStateChangeException {
        
    }
}

class collThread extends Thread {
    private Thread t;
    private String tName;
    private Concentrator C;
    //int n = 2;

    public collThread(Concentrator coll, String tName) {
        this.tName = tName;
        this.C = coll;
    }
    
    public void run() {
        try {
            C.collector();
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }
    
    public void start (){
        if (t==null){
            t=new Thread(this, tName);
            t.start ();
        }
    }
    
}
